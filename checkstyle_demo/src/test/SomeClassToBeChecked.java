/*
 * Copyright (c) 2001-2008 Beijing BidLink Info-Tech Co., Ltd.
 * All rights reserved
 * Created on 2008-2-22
 * $Id: learn_in_5_min.xml,v 1.3 2008/03/03 03:43:44 Administrator Exp $
 */
package test;

public class SomeClassToBeChecked {
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        new SomeClassToBeChecked().method();
    }
    
    
    /**
     * 方法
     */
    public void method() {
        String s = "";
        System.out.println("elloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHell12" + s);
        if (true) {
//            if (true) {
//                if (true) {
//                    if (true) {
//                    }
//                }
//            }
        }
        if (true) {
            if (true) {
                if (true) {
                    if (true) {
//                        System.out.println("");
                        System.out.print("");
                    }
                }
            }
        }
        if (true) {
            if (true) {
                if (true) {
                    if (true) {
                    }
                }
            }
        }
        if (true) {
            if (true) {
                if (true) {
                    if (true) {
                    }
                }
            }
        }
        if (true) {
            if (true) {
                if (true) {
                    if (true) {
                    }
                }
            }
        }
        if (true) {
            if (true) {
                if (true) {
                    if (true) {
                    }
                }
            }
        }
        if (true) {
            if (true) {
                if (true) {
                    if (true) {
                    }
                }
            }
        }
        if (true) {
            if (true) {
                if (true) {
                    if (true) {
                    }
                }
            }
        }
    }
    
    /**
     * 方法2
     * @param a 参数a
     */
    public void method2(int a, String str, String str1, String str2, String str3) {
        System.out.println("Hello");
        if (2 > a) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
    
}